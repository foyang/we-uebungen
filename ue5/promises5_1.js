window.onload = function(){
  var textDoc = document.getElementById("textBlock");
  
  async function fechtAData(){
    const response = await fetch('http://localhost:8080/textFiles/A.txt');
    return await response.text()
  }

  async function fechtBData(){
    const response = await fetch('http://localhost:8080/textFiles/B.txt');
    return await response.text()
  }

  fechtAData().then(textA => {
    textDoc.innerHTML = textA;
  })

  fechtBData().then(textB => {
    textDoc.innerHTML += textB;
  })
}
