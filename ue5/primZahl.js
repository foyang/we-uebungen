window.onload = function() {
  var n = 10;
  var loadIndex = 0;
  var currentIndex = 0;
  var startIindex = 1;
  var list = document.getElementById('listItem');
  var labelStart = document.getElementById('labelStart');

  labelStart.innerHTML = "Primzahlen von 1 bist " + n;

  const isPrime = n => {
    if (n === 2 || n === 3) return true;
    if (n < 2 || n % 2 === 0) return false;

    return isPrimeRecursive(n);
  }

  const isPrimeRecursive = (n, i = 3, limit = Math.floor(Math.sqrt(n))) => {
    if (n % i === 0) return false;
    if (i >= limit) return true;
    return isPrimeRecursive(n, i += 2, limit);
  }

  var i = setInterval(function() {
    if (startIindex <= n) {
      var listItem = document.createElement('li');
      var currentResult = isPrime(startIindex);
      listItem.innerHTML = "Ist " + startIindex + "ein primZahl? => " + currentResult;
      list.appendChild(listItem);
    }
    if (startIindex === n) {
      clearInterval(i);
    }
    startIindex++;
    loadIndex = n * currentIndex;
    currentIndex++;
    loadBar(loadIndex);
  }, 3000);

  var i = 0;
  function loadBar(loadIndex) {
    if (i == 0) {
      i = 1;
      var elem = document.getElementById("loadBar");
      var width = loadIndex;
      var id = setInterval(frame, 10);
      function frame() {
        if (width >= 100) {
          clearInterval(id);
          i = 0;
        } else {
          width++;
          elem.style.width = loadIndex + "%";
        }
      }
    }
  }

}
