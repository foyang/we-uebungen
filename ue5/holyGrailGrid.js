window.onload = function(){

  var jsonValue;
  var divContent = document.getElementById('content');
  var btnGroup = document.getElementById('headerBtn');

  async function fechtAData() {
    const response = await fetch('http://localhost:8080/textFiles/navigator_contents.json');
    return await response.text()
  }

  fechtAData().then(textA => {
    jsonValue = textA;
  })

function showJavaScript(){
  var javaScritContent = jsonValue['javascript'];
  var functionContent = javaScritContent['function'];
  var jsContent = functionContent['content'];

  var currentActiv = btnGroup;
  currentActiv[0].className = currentActiv[0].className.replace("active");
  divContent.innerHTML = jsContent;

}

function showCss(){
  var csssContent = jsonValue['css'];
  var seclectorContent = csssContent['selectors'];
  var jsContent = seclectorContent['content'];
  currentActiv[0].className = currentActiv[0].className.replace("active");
  divContent.innerHTML = jsContent;
}

}
