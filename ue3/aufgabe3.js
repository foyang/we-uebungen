// Aufgabe 3*/
function identity_function(arg){
  return function(){
    return arg;
  };
}
function addf(x){
  return function(y){
    return x + y;

  }
}
function applyf(funktionName){
  return  function(x){
    return funktionName(x);
  }
}
function mull(x){
  return function(y){
    return x * y;
  }
}
console.log("Test Aufgabe 3.1 => 3:  "+applyf(mull)(5)(6));
function curry(funk, x){
  return funk(x);
}
console.log("Test Aufgabe 3.1 => 4:  "+curry(mull, 5)(6));
var inc_add = addf(1);
var inc_curry = curry(addf, 1);
var inc_applyf = applyf(addf)(1);
function methodize(funkt) {
  return function(x) {
    return funkt(x)
  }
}
function twice(funkt) {
  return function(x) {
    return funkt(x, x)
  }
}
function composeu(funkt1, funkt2) {
    return function(x) {
      return funkt2(funkt1(x));
    }
 }
 function composeb(funkt1, funkt2) {
  return function(x, y, z) {
    return funkt2(funkt1(x, y), z);
  }
 }
 var tour = false;
 function once(funkt){
   if (!tour) {
     tour = true;
     return function(x){
       funkt(x);
     }
   } else {
     console.error("kann nicht mehr als auf einmal ausführen.");
   }
   tour = false;
 }
 function revocable(funkt) {
   return {
     invoke: function (x, y) {
       if (funkt !== undefined ) {
         return funkt(x, y);
       }
     },
     revoke: function () {
       funkt = undefined;
       console.error("Fehlerabbruch!");
     }
   }
 }
 function wrapperFunk() {
   var arrayStart = [];
   return {
     get: function get(i) {
       return arrayStart[i];
     },
     append: function append(v) {
       arrayStart.push(v);
     },
     store: function store(i, v) {
       arrayStart[i] = v;
     }
   };
 }
 function factoryFunkt(x) {
   var unique = increFunk(0);
   return function() {
     return x + unique();
   }
 }
 function increFunk(x) {
  return function() {
    var finalResult = x;
    x += 1;
    return finalResult;
  }
 }
 var gensym = factoryFunkt('G');
 console.log("#####: "+gensym());
 console.log("#####: "+gensym());
 console.log("#####: "+gensym());
 console.log("#####: "+gensym());
 function fibonaccif(x, y) {
   return function() {
     var nextFibo = x;
     x = y;
     y += nextFibo;
     return nextFibo;
   }
 }
 var fib = fibonaccif(0, 1);
 console.log("#####: "+fib());
 console.log("#####: "+fib());
 console.log("#####: "+fib());
 console.log("#####: "+fib());
 console.log("#####: "+fib());
 console.log("#####: "+fib());
 function addg(firstNumber) {
   function addOneMore(nextNumber) {
     if (nextNumber === undefined ) {
       return firstNumber;
     }
     firstNumber += nextNumber;
     return addOneMore;
   }
   if (firstNumber !== undefined ) {
     return addOneMore;
   }
 }
 console.log("#####: "+addg(3)(4)(5)());
 console.log("#####: "+addg(1)(2)(4)(8)());
 function applyg(x){
  return function(y){
    return x(y);
  }
}
console.log("#####: "+applyg(addg)(3)(4)(5)());
console.log("#####: "+applyg(addg)(1)(2)(4)(8)());
function m(x, y) {
  return {
    value: x,
    source: (typeof y === 'string')?y:String(x)
  };
}
console.log("#####: "+JSON.stringify(m(1)));
console.log("#####: "+JSON.stringify(m(Math.PI, "pi")));
function addm(x, y) {
  var value = x.value + y.value
  var source = '(' + x.source + '+' + y.source + ')'
  return m(value, source)
}
console.log("#####: "+JSON.stringify(addm(m(3), m(4))));
function binarymf(biFunkt, text) {
  return function(x, y) {
    var value  = biFunkt(x.value, y.value),
        source = '(' + x.source + text + y.source + ')'
    return m(value, source);
  }
}
var addm = binarymf(add, "+");
console.log("#####: "+JSON.stringify(addm(m(3), m(4))));
function biFunkt(biFunkt, z) {
  return function(x, y) {
    if( typeof x === 'number' ) { x = m(x) }
    if( typeof y === 'number' ) { y = m(y) }
    return m(
      biFunkt(x.value, y.value),
      '(' + x.source + z + y.source + ')'
    );
  }
}
var addm = biFunkt(add, "+");
console.log("#####: "+JSON.stringify(addm(3, 4)));
function exp(numb) {
  var biFunct;
  var x;
  var y;
  if (typeof numb === 'number' ) {
     return numb
   }
  biFunct = numb[0],
  x = numb[1],
  y = numb[2]
  return biFunct(x, y)
}
var hypa = [ Math.sqrt, [ add, [mul, 3, 3], [mul, 4, 4] ] ];
console.log("#####: "+exp(hypa));
var variable;
function store(x){
  return variable = x;
}
console.log("####: "+store(5));
function identityf(arg) {
  return function() {
    return arg;
  }
}
function squatre(x , y, w,z){
  return function(x){
    var p = x(y,w);
    return z(p);
  }
}
console.log("####+++**: "+squatre( add, identityf(3), identityf(4), store ));
function unaryc(unFunc){
  return function(callback, x) {
    return callback(unFunc(x));
  }
}
var sqrtc = unaryc(Math.sqrt);
console.log("###****#####***: "+sqrtc(81, store));
