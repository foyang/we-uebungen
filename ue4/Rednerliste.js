
var myVar;
var timeStatus = true;

function delteItem(newDiv) {
  document.getElementById(newDiv.id).remove();
}
function removAllItem() {
  var list = document.getElementById('listItem');
  list.innerHTML = '';
}
function endTime(myVar) {
  clearInterval(myVar);
}

window.onload = function() {
  var idIndex = 0;
  var formBlock = document.getElementById('formItem');
  var list = document.getElementById('listItem');
  var deleteAll = document.getElementById('deleteAllBtn');

  formBlock.onsubmit = function(evt) {

    if (idIndex != 0) {
      var previewDiv = document.getElementById('seconds' + (idIndex - 1));
      var previewBtn = document.getElementById('btn-' + (idIndex - 1));
      endTime();
      clearInterval(myVar);
      previewBtn.textContent = 'Start!';
    }
    evt.preventDefault();

    var listItem = document.createElement('li');
    var listBtn = document.createElement('button');
    var newDiv = document.createElement("div");
    var hourLabel = document.createElement("label");
    var minLabel = document.createElement("label");
    var secondLabel = document.createElement("label");
    var spanLabel = document.createElement("span");
    var spanLabel2 = document.createElement("span");

    secondLabel.setAttribute('id', 'seconds' + idIndex);
    minLabel.setAttribute('id', 'minutes' + idIndex);
    hourLabel.setAttribute('id', 'hour' + idIndex);
    newDiv.setAttribute('id', 'itemDiv' + idIndex);
    listBtn.setAttribute('id', 'btn-' + idIndex);

    secondLabel.textContent = '00';
    minLabel.textContent = '00';
    hourLabel.textContent = '00';
    listBtn.textContent = 'Stopp!';
    spanLabel.textContent = ":";
    spanLabel2.textContent = ":";

    var itemName = document.getElementById('itemName').value;
    listItem.appendChild(document.createTextNode(itemName));

    newDiv.appendChild(listItem);
    newDiv.appendChild(hourLabel);
    newDiv.appendChild(spanLabel2);
    newDiv.appendChild(minLabel);
    newDiv.appendChild(spanLabel);
    newDiv.appendChild(secondLabel);
    newDiv.appendChild(listBtn);

    startTime();
    list.appendChild(newDiv);
    document.getElementById('itemName').value = '';
    
    listBtn.addEventListener("click", function() {
      if (timeStatus) {
        endTime(myVar);
        listBtn.textContent = 'Start!';
        timeStatus = false;
      } else {
        startTime();
        timeStatus = true;
        listBtn.textContent = 'Stopp!';
      }
    })
    idIndex++;
    if (idIndex != 0) {
      deleteAll.style.display = 'block';
    }

    function pad(val) {
      var valString = val + "";
      if (valString.length < 2) {
        return "0" + valString;
      } else {
        return valString;
      }
    }
    var totalSeconds = 0;

    function setTime() {
      ++totalSeconds;
      secondLabel.innerHTML = pad(totalSeconds % 60);
      minLabel.innerHTML = pad(parseInt(totalSeconds / 60));
      hourLabel.innerHTML = pad(parseInt(totalSeconds / 3600));
    }

    function startTime() {
      myVar = setInterval(setTime, 1000);
    }

  }
}
