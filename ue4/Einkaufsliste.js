
window.onload = function(){

  var formBlock = document.getElementById('formItem');
  var list = document.getElementById('listItem');
  var deleteAll = document.getElementById('deleteAllBtn');
  var idIndex = 0;

  formBlock.onsubmit = function(evt) {
    evt.preventDefault();
    var listItem = document.createElement('li');
    var listBtn = document.createElement('button');
    var newDiv = document.createElement("div");
    newDiv.setAttribute('id','itemDiv'+idIndex);

    listBtn.textContent = 'Delete';
    var itemName = document.getElementById('itemName').value;
    listItem.appendChild(document.createTextNode(itemName));

    newDiv.appendChild(listItem);
    newDiv.appendChild(listBtn);

    list.appendChild(newDiv);
    document.getElementById('itemName').value='';
    listBtn.addEventListener("click",function(){
      delteItem(newDiv);
    })
    idIndex++;
    if(idIndex != 0){
      deleteAll.style.display = 'block';
    }
  }
}
function delteItem(newDiv){
  document.getElementById(newDiv.id).remove();
}
function removAllItem(){
  var list = document.getElementById('listItem');
  list.innerHTML = '';
}
