import Vue from 'vue'
import App from './App.vue'
import router from './router'


import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    activComponent: ''
  },
  mutations: {
    setComponent (state, payload) {
      state.activComponent = payload;
    }
  },
  actions: {
    setComponent(context, payload) {
      context.commit('setComponent', payload);
    }
  },
  getters: {
    getActivComponent(state) {
      return state.activComponent;
    }
  }
});

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.config.productionTip = false;
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
