import Vue from 'vue'
import VueRouter from 'vue-router'
// import showCSS from '../../src/components/ShowCss'
import StartComponent from '../components/StartComponent'
import CssComponent from '../components/CssComponent'
import HtmlComponent from '../components/HtmlComponent'
import JavaScripComponent from '../components/JavaScripComponent'
import OtherComponent from '../components/OtherComponent'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'startComponent',
    component: StartComponent
  },
  {
    path: '/css',
    name: 'cssComponent',
    component: CssComponent
  },

  {
    path: '/html',
    name: 'htmlComponent',
    component: HtmlComponent
  },

  {
    path: '/javaScript',
    name: 'javaScripComponent',
    component: JavaScripComponent
  },

  {
    path: '/other',
    name: 'otherComponent',
    component: OtherComponent
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  // base: '/ue9VueJs/',
  routes
})

export default router
