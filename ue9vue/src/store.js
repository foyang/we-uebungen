export default {
    state: {
        activComponent: ''
    },
    mutations: {
        setComponent (state, payload) {
            state.activComponent = payload;
        }
    },
    actions: {
        setComponent(context, payload) {
            context.commit('setComponent', payload);
        }
    },
    getters: {
        getActivComponent(state) {
            return state.activComponent;
        }
    }
}
