import SitebareLeft from "./SitebareLeft";
import './sitebareLeft.css'
export default {
    title: 'SitebareLeft',
    component: SitebareLeft
};

const Template = (args, { argTypes }) => ({
    props: Object.keys(argTypes),
    components: {SitebareLeft},
    template: '      <div class="sidebar-1">\n' +
        '        <div class="button-groupe">\n' +
        '          <button type="button" onclick="showPromise()">Promise</button>\n' +
        '          <button type="button" onclick="showAsync()">async</button>\n' +
        '          <button type="button" onclick="showFetch()">fetch</button>\n' +
        '          <button type="button" onclick="showCallback()">callback</button>\n' +
        '          <button type="button" onclick="showClass()">class</button>\n' +
        '\n' +
        '        </div>\n' +
        '      </div>\n'
});

export const SiteBarLeft = Template.bind({});
