import Navigation from '../components/shared/Navigation';
import './navigation.css'

export default {
    title: 'Navigation',
    component: Navigation,
};

const Template = (args, { argTypes }) => ({
    props: Object.keys(argTypes),
    components: { Navigation },
    template:
        '    <div id="headerBtn" class="header">\n' +
        '        <center><h2>WWW-Navigator</h2></center>\n' +
        '        <div class="button-groupe">\n' +
        '            <router-link to="/html">\n' +
        '                <button>HTMTL</button>\n' +
        '            </router-link>\n' +
        '            <router-link to="/css">\n' +
        '                <button>CSS</button>\n' +
        '            </router-link>\n' +
        '            <router-link to="/javaScript">\n' +
        '                <button>JavaScript</button>\n' +
        '            </router-link>\n' +
        '            <router-link to="/other">\n' +
        '                <button>Other</button>\n' +
        '            </router-link>\n' +
        '        </div>\n' +
        '    </div>'+ '<style> .button{background-color: red}</style>',
});


export const navBar = Template.bind({});

