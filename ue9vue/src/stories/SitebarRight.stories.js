import SitebarRight from "../components/shared/SitebarRight";
import './sitebarRight.css'
export default {
    title: 'SitebarRight',
    component: SitebarRight,
};

const Template = (args, { argTypes }) => ({
    props: Object.keys(argTypes),
    components: {SitebarRight},
    template: '<div class="sidebar-2">Additional<br> Information: Links to external ressources</div>'
});

export const SiteBarRight = Template.bind({});
