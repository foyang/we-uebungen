import FooterComponent from "../components/shared/FooterComponent";
import './footerComponent.css'
export default {
    title: 'FooterComponent',
    component: FooterComponent,
};

const Template = (args, { argTypes }) => ({
    props: Object.keys(argTypes),
    components: {FooterComponent},
    template: '    <div class="footer">\n' +
        '        <span>Footer:</span>\n' +
        '        <ul>\n' +
        '            <li>Sitemap</li>\n' +
        '            <li>Home</li>\n' +
        '            <li>News</li>\n' +
        '            <li>Contact</li>\n' +
        '            <li>About</li>\n' +
        '        </ul>\n' +
        '    </div>'
});

export const SiteBarRight = Template.bind({});
