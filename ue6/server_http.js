let http = require('http');
let fs = require('fs')
let url = require('url')
let server_http = http.createServer()

server_http.on('request',(request, response) => {
    response.writeHead(200)
    let query = url.parse(request.url,true).query
    let name = query.username === undefined ? 'anonyme' : query.username;

    fs.readFile('index.html','utf8',(err,data) => {
        if (err) {
            response.writeHead(404)
            response.end('Data not found')
        } else {
            response.writeHead(200, {
                'content-type': 'text/html; charset=utf-8'
            })
            data = data.replace('{{name}}',name)
            response.end(data)
        }
    })
})

server_http.listen(8080)
