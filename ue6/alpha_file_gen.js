const LoremIpsum = require("lorem-ipsum").LoremIpsum;
let fs = require('fs')
let wrap = require('word-wrap');
var param = process.argv.slice(2)
var finalString=[]

const lorem = new LoremIpsum({
    sentencesPerParagraph: {
        max: 8,
        min: 4
    },
    wordsPerSentence: {
        max: 16,
        min: 4
    }
})
function gerenratText(textSize) {
    var stringText = wrap(lorem.generateSentences(textSize),{width: 100}).split('\n');
    return stringText;
}


class StringIdGenerator {
    constructor(chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        this._chars = chars;
        this._nextId = [0];
    }

    next() {
        const r = [];
        for (const char of this._nextId) {
            r.unshift(this._chars[char]);
        }
        this._increment();
        return r.join('');
    }

    _increment() {
        for (let i = 0; i < this._nextId.length; i++) {
            const val = ++this._nextId[i];
            if (val >= this._chars.length) {
                this._nextId[i] = 0;
            } else {
                return;
            }
        }
        this._nextId.push(0);
    }

    *[Symbol.iterator]() {
        while (true) {
            yield this.next();
        }
    }
}


function many() {
    var currentText = gerenratText(parseInt(param,10))
    const ids = new StringIdGenerator();
    for (var i=0; i<currentText.length; i++){
        finalString.push(ids.next()+"."+currentText[i]+'\n');
    }
}
many()

fs.writeFile("data/alpha_file_gen.txt",finalString.toString().replace(/,/g, ''),{width: 100},function(err) {
    if(err){
        return console.log(err)
    } else {
        console.log('The file was saved!')
    }
})

