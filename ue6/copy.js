let fs = require('fs')
let file = "demo.txt"


fs.stat(file,(err, stat) => {
    let total = stat.size
    let progress = 0

    let readStrem = fs.createReadStream(file)
    let writeStream = fs.createWriteStream('copy.text')

    readStrem.on('data',(chunk) => {
        progress += chunk.length

        console.log("Lecture okay "+Math.round(100 * progress / total) + "%")
    })

    readStrem.pipe(writeStream)

    writeStream.on('finish',() => {
        console.log('Fichier bien Copier')
    })
})

