const fs = require('fs')
var finalString=[]
let lines1 = []
let lines2 = []

function writeMerge(finalString){
    fs.writeFile("data/merge_fs.txt",finalString.toString().replace(/,/g, ''),function(err) {
        if(err){
            return console.log(err)
        } else {
            console.log('The file was saved!')
        }
    })
}

fs.readFile('data/alpha_file_gen.txt',callback(lines1))
fs.readFile('data/number_file_gen.txt',callback(lines2))

function callback(lines) {
    return function (err, data) {
        lines.push(...data.toString().split('\n'))
        if (lines1.length && lines2.length){
            lines1.forEach((line, index) =>  finalString.push(line+lines2[index]))
        }
        writeMerge(finalString);
    }
}

