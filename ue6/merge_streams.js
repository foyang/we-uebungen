const fs = require('fs')

let lines1 = []
let lines2 = []
var finalString=[]

function writeMerge(finalString){
    fs.writeFile("data/merge_stream.txt",finalString.toString().replace(/,/g, ''),function(err) {
        if(err){
            return console.log(err)
        } else {
            console.log('The file was saved!')
        }
    })
}

const stream1 = fs.createReadStream('data/alpha_file_gen.txt')
const stream2 = fs.createReadStream('data/number_file_gen.txt')


let count = 0
function output(){
    count +=1;
    if (count === 2){
        lines1.forEach((line,i) => finalString.push(line,lines2[i])
    )
        writeMerge(finalString)
    }
}

function process_chunk(lines){
    return function (chunk) {
        let i=0
        chunk.toString().split('\n').forEach(function (line) {
            if (!lines[i])
                lines[i]=""
            lines[i++] +=line;
        })
    }
}
stream1.on('data', process_chunk(lines1))
stream2.on('data', process_chunk(lines2))

stream1.on('end', output)
stream2.on('end', output)
