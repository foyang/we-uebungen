const LoremIpsum = require("lorem-ipsum").LoremIpsum;
let fs = require('fs')
let wrap = require('word-wrap');
var param = process.argv.slice(2)
var finalString=[]

const lorem = new LoremIpsum({
    sentencesPerParagraph: {
        max: 8,
        min: 4
    },
    wordsPerSentence: {
        max: 16,
        min: 4
    }
})
function gerenratText(textSize) {
    var stringText = wrap(lorem.generateSentences(textSize),{width: 100}).split('\n');
    return stringText;
}
function many() {
    var currentText = gerenratText(parseInt(param,10))
        for (var i=0; i<currentText.length; i++){
            finalString.push(i+"."+currentText[i]+'\n');
    }
}
many()

fs.writeFile("data/number_file_gen.txt",finalString.toString().replace(/,/g, ''),{width: 100},function(err) {
    if(err){
        return console.log(err)
    } else {
        console.log('The file was saved!')
    }
})

