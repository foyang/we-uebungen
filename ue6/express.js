const express = require('express');
const multer = require('multer');
const fs = require('fs')

var uploadFiles = []
var finalString = []
let lines1 = []
let lines2 = []

const upload = multer({
    dest: 'uploads/'
});

const app = express();

app.get('/', (req, res) => {
    res.sendFile( __dirname +'/index.html');
});

app.get('/download',function(req,res){
    res.download(__dirname +'/data/merge_fs.txt','merge_fs.txt');
})

app.post('/', upload.any('file-to-upload'), (req, res) => {
    uploadFiles = req.files
    if (uploadFiles.length !== 0){
        fs.readFile('uploads/'+uploadFiles[0]['filename'],callback(lines1))
        fs.readFile('uploads/'+uploadFiles[1]['filename'],callback(lines2))
    }
    res.redirect('/');
});

function writeMerge(finalString){
    fs.writeFile("data/merge_fs.txt",finalString.toString().replace(/,/g, ''),function(err) {
        if(err){
            return console.log(err)
        } else {
            console.log('The file was saved!')
        }
    })
}

function callback(lines) {
    return function (err, data) {
        lines.push(...data.toString().split('\n'))
        if (lines1.length && lines2.length){
            lines1.forEach((line, index) =>  finalString.push(line+lines2[index]))
        }
        writeMerge(finalString);
    }
}

app.listen(3000);
