window.onload = function(){

  class Tabellenkalkulation extends HTMLElement{
    constructor() {
      super();
     this.attachShadow({mode:'open'});
      var index = 0;

      this.headline = document.createElement('h1');
      this.headlineTxt = this.getAttribute('headLine');
      this.headline.innerHTML = this.headlineTxt;
      this.appendChild(this.headline)

      var addBtn = document.createElement('button');
      addBtn.setAttribute('id','addBtn');
      addBtn.textContent = this.getAttribute('btnName');

      var tableBlock = document.createElement('table');
      tableBlock.setAttribute('id','table');
      tableBlock.style.width = '50%'

      var tableBody = document.createElement('tbody');
      tableBlock.setAttribute('id','table-block');

      var tableTr = document.createElement('tr');
      tableTr.setAttribute('id','table-block-cell');
      var tableTh = document.createElement('th');
      tableTh.setAttribute('contenteditable',true);
      tableTh.textContent = 'Sum';
      var tableTd = document.createElement('td');
      tableTd.setAttribute('contenteditable',true);
      tableTd.textContent = 0;

      this.appendChild(addBtn);
      this.appendChild(tableBlock);
      tableBlock.appendChild(tableBody);
      tableBody.appendChild(tableTr);
      tableTr.appendChild(tableTh);
      tableTr.appendChild(tableTd);

      addBtn.addEventListener('click',function(){
        var tr = document.createElement('tr');
        var th = document.createElement('th');
        var td = document.createElement('td');

        tr.setAttribute('id','tab-tr-'+index);
        th.setAttribute('id','tab-th-'+index);
        td.setAttribute('id','tab-td-'+index);
        th.setAttribute('contenteditable',true);
        td.setAttribute('contenteditable',true);

        th.textContent = "Name =";
        td.textContent = "0";
        tr.appendChild(th);
        tr.appendChild(td);
        tableBody.appendChild(tr);
      });
this.shadowRoot.appendChild(document.querySelector('tabellen-kalkulation').content)
     // var tt = document.getElementById("table").rows[0].cells[0].getElementsByTagName("td")[0].value;
    }
  }
  customElements.define('tabellen-kalkulation', Tabellenkalkulation)
}
