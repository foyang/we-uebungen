window.onload = function(){
    class Einkaufsliste extends HTMLElement{
        constructor() {
            super();
            // this.attachShadow({mode:'open'});

            var idIndex = 0;

            this.headline = document.createElement('h1');
            this.headlineTxt = this.getAttribute('headLine');
            this.headline.innerHTML = this.headlineTxt;

            this.formBlock = document.createElement('form');
            this.formBlock.setAttribute('id','formItem');

            this.inputLabel = document.createElement('label');
            this.inputLabel.innerText = 'Enter a new item: ';

            this.inputTxt = document.createElement('input');
            this.inputTxt.setAttribute('type','txt');
            this.inputTxt.setAttribute('id','itemName');
            this.inputTxt.setAttribute('name','itemName');

            this.subBtn = document.createElement('button');
            this.subBtn.innerText = 'Add item';

            const olList = document.createElement('ol');
            olList.setAttribute('id','listItem');

            const deleteAllBtn = document.createElement('button');
            deleteAllBtn.setAttribute('id','deleteAllBtn');
            deleteAllBtn.textContent = 'Delete all items';

            this.appendChild(this.headline);
            this.appendChild(this.formBlock);
            this.formBlock.appendChild(this.inputLabel);
            this.formBlock.appendChild(this.inputTxt);
            this.formBlock.appendChild(this.subBtn);
            this.appendChild(olList);
            this.appendChild(deleteAllBtn);

            this.formBlock.onsubmit = function(evt) {
                evt.preventDefault();
                var listItem = document.createElement('li');
                var listBtn = document.createElement('button');
                var newDiv = document.createElement("div");
                newDiv.setAttribute('id','itemDiv'+idIndex);

                listBtn.textContent = 'Delete';
                var itemName = document.getElementById('itemName').value;
                listItem.appendChild(document.createTextNode(itemName));

                newDiv.appendChild(listItem);
                newDiv.appendChild(listBtn);

                olList.appendChild(newDiv);
                document.getElementById('itemName').value='';
                listBtn.addEventListener("click",function(){
                    delteItem(newDiv);
                })
                idIndex++;
                if(idIndex !== 0){
                    deleteAllBtn.style.display = 'block';
                }
            }
            deleteAllBtn.addEventListener('click',function () {
                removAllItem();
            })
        }
    }
    customElements.define('einkaufs-list', Einkaufsliste)
}
function delteItem(newDiv){
    document.getElementById(newDiv.id).remove();
}
function removAllItem(){
    var list = document.getElementById('listItem');
    list.innerHTML = '';
}
