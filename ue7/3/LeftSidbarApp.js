export class LeftSidbarApp extends LitElement {
    static get styles() {
        return css`
        
    .container {
      height: 100vh;
      width: 100vw;
      /* display: grid; */
      grid-template-columns: 1fr;
      grid-template-rows: 90px 1fr 45px;
      background-color: #000000;
    }
    button {
      width: 7em;
      height: 2em;
      margin-right: 5px;
      margin-left: 5px;
      border-radius: 40px;
      background-color: #6A709E;
      border: 3px solid #c1c1c1;
      color: #000000;
      cursor: pointer;
    }

    .button-groupe {
      margin-top: -1em;
    }

    .sidebar {
      border-right: 1px solid lightgrey;
    }
    .body {
      display: grid;
      grid-template-columns: 1fr 3fr;
      overflow: hidden;
    }
    .content {
      overflow-y: scroll;
      padding: 20px;
      background-color: #6A9EBD;
      color: #fff;
      text-align: center;
      margin: 0;
      padding: 0;
    }

    .sidebar-1 {
      background-color: #C28282;
      color: #fff;
      text-align: center;
      word-break: break-all;
      font-size: 2em;
    }

    .sidebar-1 .button-groupe {
      display: grid;
      margin-top: 1em;
    }

    .sidebar-1 .button-groupe button {
      margin-bottom: 1em;
      /*font-style: .5em !important;*/
    }
    `;
    }

    static get properties() {
        return {
            sidebarLeft: {type: Array},
        };
    }

    constructor() {
        super();
        this.sidebarLeft = ["Promise", "async", "fetch", "callback", "class"];
    }

    render() {
        return html`
  <div class="container">
    <div class="body">
      <div class="sidebar-1">
        <div class="button-groupe">
          ${this.sidebarLeft.map(
            item => html`
          <button @click=${this._showLeftSidbarMenuContent} class="active" part="button">
          ${item}
          </button>
            `
        )}
        </div>
      </div>
      <div id="content" class="content">
      </div>
    </div>
    </div>
  </div>
    `;
    }

    _showLeftSidbarMenuContent(item) {
    }
}

window.customElements.define('www-sidebarleft', LeftSidbarApp);
