import {LitElement, html, css} from 'lit-element';

export class NavbarApp extends LitElement {
    static get styles() {
        return css`
        body {
      overflow: hidden;
      margin: 0 !important;
    }

    .container {
      height: 100vh;
      width: 100vw;
      display: grid;
      grid-template-columns: 1fr;
      grid-template-rows: 90px 1fr 45px;
      background-color: #000000;
    }

    .header {
      background-color: #C14F4F;
      color: #fff;
    }

    .header>h2 {
      text-align: center;
    }

    button {
      width: 7em;
      height: 2em;
      margin-right: 5px;
      margin-left: 5px;
      border-radius: 40px;
      background-color: #6A709E;
      border: 3px solid #c1c1c1;
      color: #000000;
      cursor: pointer;
    }

    .button-groupe {
      margin-top: -1em;
    }

    .body {
      display: grid;
      grid-template-columns: 1fr 3fr 1fr;
      overflow: hidden;
    }

    .content {
      overflow-y: scroll;
      padding: 20px;
      background-color: #6A9EBD;
      color: #fff;
      text-align: center;
      margin: 0;
      padding: 0;
    }
    `;
    }

    static get properties() {
        return {
            menuHeadLine: {type: String},
            menuItem: {type: Array},
        };
    }

    constructor() {
        super();
        this.menuHeadLine = 'WWW-Navigator';
        this.menuItem = ["HTML", "CSS", "JavaScript", "Other"];
    }

    render() {
        return html`
  <div class="container">
    <div id="headerBtn" class="header">
      <h2>${this.menuHeadLine}</h2>
      <div class="button-groupe">
        ${this.menuItem.map(
            item => html`
          <button @click=${() => this._showMenuContent(item)} class="active" part="button">
          ${item}
          </button>
            `
        )}
      </div>
    </div>
  </div>
    `;
    }
    _showMenuContent(item) {}
}

window.customElements.define('www-navbar', NavbarApp);
