
import {LitElement, html, css} from 'lit-element';

export class FooterComponent extends LitElement {
    static get styles() {
        return css`
    body {
      overflow: hidden;
      margin: 0;
    }
    .container {
      height: 100vh;
      width: 100vw;
      display: grid;
      grid-template-columns: 1fr;
      grid-template-rows: 90px 1fr 45px;
      background-color: #000000;
    }
    .footer {
      background-color: #000;
      color: #fff;
      display: -webkit-inline-box;
      justify-content: center;
      margin: auto;
    }

    .footer ul {
      list-style-type: none;
      padding: 0;
    }

    .footer li {
      float: left;
      margin-left: .5em;
      text-decoration: underline;
    }
    .footer>span {
      font-size: 2em;
    }
    `;
    }

    static get properties() {
        return {
            menuHeadLine: {type: String},
            menuItem: {type: Array},
        };
    }

    constructor() {
        super();
        this.footerHeadLine = 'Footer';
        this.footerItem = ["Sitemap", "Home", "News", "Contact", "About"];
    }

    render() {
        return html`
  <div class="container">
    <div class="footer">
      <span>${this.footerHeadLine}:</span>
      <ul>
      ${this.footerItem.map(
            item => html`
           <li>${item}</li>
          `
        )}
      </ul>
    </div>
  </div>
    `;
    }
}
window.customElements.define('www-footer', FooterComponent);

