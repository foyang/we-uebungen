import {LitElement, html, css} from 'lit-element';

export class NavbarApp extends LitElement {
  static get styles() {
    return css`
        body {
      overflow: hidden;
      margin: 0 !important;
    }

    .container {
      height: 100vh;
      width: 100vw;
      display: grid;
      grid-template-columns: 1fr;
      grid-template-rows: 90px 1fr 45px;
      background-color: #000000;
    }

    .header {
      background-color: #C14F4F;
      color: #fff;
    }

    .header>h2 {
      text-align: center;
    }

    button {
      width: 7em;
      height: 2em;
      margin-right: 5px;
      margin-left: 5px;
      border-radius: 40px;
      background-color: #6A709E;
      border: 3px solid #c1c1c1;
      color: #000000;
      cursor: pointer;
    }

    .button-groupe {
      margin-top: -1em;
    }

    .sidebar {
      border-right: 1px solid lightgrey;
    }

    .footer {

      background-color: #000;
      color: #fff;
      display: -webkit-inline-box;
      justify-content: center;
      margin: auto;
    }

    .footer ul {
      list-style-type: none;
      padding: 0;
    }

    .footer li {
      float: left;
      margin-left: .5em;
      text-decoration: underline;
    }

    .footer>span {
      font-size: 2em;
    }

    .body {
      display: grid;
      grid-template-columns: 1fr 3fr 1fr;
      overflow: hidden;
    }

    .content {
      overflow-y: scroll;
      padding: 20px;
      background-color: #6A9EBD;
      color: #fff;
      text-align: center;
      margin: 0;
      padding: 0;
    }

    .sidebar-1,
    .sidebar-2 {
      background-color: #C28282;
      color: #fff;
      text-align: center;
      word-break: break-all;
      font-size: 2em;
    }

    .sidebar-1 .button-groupe {
      display: grid;
      margin-top: 1em;
    }

    .sidebar-1 .button-groupe button {
      margin-bottom: 1em;
      /*font-style: .5em !important;*/
    }
    `;
  }

  static get properties() {
    return {
      menuHeadLine: {type: String},
      sidebarRigth: {type: String},
      count: {type: Number},
      menuItem: {type: Array},
      sidebarLeft: {type: Array},
    };
  }

  constructor() {
    super();
    this.menuHeadLine = 'WWW-Navigator';
    this.count = 0;
    this.menuItem = ["HTML", "CSS", "JavaScript", "Other"];
    this.sidebarLeft = ["Promise", "async", "fetch", "callback", "class"];
    this.sidebarRigth = "Additional Information: Links to external ressources";
  }

  render() {
    return html`
  <div class="container">
    <div id="headerBtn" class="header">
      <h2>${this.menuHeadLine}</h2>
      <div class="button-groupe">
        ${this.menuItem.map(
        item => html`
          <button @click=${this._showMenuContent} class="active" part="button">
          ${item}
          </button>
            `
    )}
      </div>
    </div>
    <div class="body">
      <div class="sidebar-1">
        <div class="button-groupe">
          ${this.sidebarLeft.map(
        item => html`
          <button @click=${this._showLeftSidbarMenuContent} class="active" part="button">
          ${item}
          </button>
            `
    )}
        </div>
      </div>
      <div id="content" class="content">

      </div>
      <div class="sidebar-2">${this.sidebarRigth}</div>
    </div>
  </div>
    `;
  }

  _showMenuContent() {}
  _showLeftSidbarMenuContent() {}
}

window.customElements.define('www-navigation', NavbarApp);
